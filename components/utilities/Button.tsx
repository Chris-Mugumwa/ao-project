import { ReactNode } from 'react'

type ButtonProps = {
	path: string
	children?: ReactNode
}

const Button = ({ path, children }: ButtonProps) => {
	return (
		<a
			href={path}
			className='mt-6 px-8 py-4 bg-orange-dark w-fit rounded-md hover:bg-transparent ring-2 ring-transparent hover:ring-orange-dark transition duration-500 text-gray-light hover:text-blue-dark dark:text-gray-light'>
			{children}
		</a>
	)
}

export default Button
