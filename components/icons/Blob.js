import * as React from "react";

const SvgBlob = (props) => (
  <svg viewBox="0 0 200 200" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path
      fill="#FB8500"
      d="M151.7 82.2c7.3 23.5-.3 50.8-19.6 65.7-19.3 14.9-50.4 17.4-67.6 4.2C47.3 138.8 44 109.7 52.2 85c8.2-24.8 28-45.3 49-45.7 21-.4 43.2 19.3 50.5 42.9Z"
    />
  </svg>
);

export default SvgBlob;
