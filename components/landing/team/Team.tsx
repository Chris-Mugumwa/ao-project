import Image from 'next/image'
import { teamData } from './'
import { Blob } from '../../icons'
import { TeamsCard, SubHeading } from '../../utilities'

const Team = () => {
	return (
		<section
			id='team'
			className='flex-col items-center justify-center page lg:py-24 bg-gray-light dark:bg-blue-darker lg:min-h-screen'>
			<SubHeading>Meet the Team</SubHeading>

			<div className='flex flex-wrap items-center mt-8'>
				<ul className='flex flex-wrap items-center justify-center mt-32 space-x-4 lg:w-1/2'>
					{teamData.map(team => (
						<TeamsCard key={team.name} team={team} />
					))}
				</ul>

				<section className='relative mt-32 lg:w-1/2 lg:mt-0'>
					<Image
						src='/Robot.png'
						alt='robot'
						width='400'
						height='500'
						priority
						className='relative z-30'
					/>
					<Blob className='absolute z-20 scale-150 -top-0 right-12' />
				</section>
			</div>
		</section>
	)
}

export default Team
