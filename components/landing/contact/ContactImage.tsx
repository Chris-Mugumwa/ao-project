import Image from 'next/image'
import { Blob } from '../../icons'

const ContactImage = () => {
	return (
		<section className='relative flex items-center justify-center w-full h-full lg:w-1/2'>
			<Image
				src='/Robot-2.png'
				alt='Robot'
				width='400'
				height='500'
				className='relative z-30'
			/>
			<Blob className='absolute z-20 scale-125 -top-10 -right-10' />
		</section>
	)
}

export default ContactImage
