import {
	MdOutlineMoneyOffCsred,
	MdOutlineAttachMoney,
	MdOutlineGridGoldenratio,
} from 'react-icons/md'

type Pricing = {
	id: number
	name: string
	icon: JSX.Element
	price: string
	perk: {
		perkOne: string
		perkTwo: string
		perkThree: string
	}
}[]

const pricingData: Pricing = [
	{
		id: 1,
		name: 'Free Package',
		icon: <MdOutlineMoneyOffCsred />,
		price: 'Free',
		perk: {
			perkOne: 'Secure your account',
			perkTwo: 'One credit card',
			perkThree: 'Customer Support',
		},
	},
	{
		id: 2,
		name: 'Basic',
		icon: <MdOutlineAttachMoney />,
		price: 'R150 p/m',
		perk: {
			perkOne: 'Secure your account',
			perkTwo: 'Three credit cards',
			perkThree: 'Customer Support',
		},
	},
	{
		id: 3,
		name: 'Premium',
		price: 'R300 p/m',
		icon: <MdOutlineGridGoldenratio />,
		perk: {
			perkOne: 'Unlimited credit cads',
			perkTwo: 'Virtual Credits',
			perkThree: 'Personal fin-expert',
		},
	},
]

export default pricingData
