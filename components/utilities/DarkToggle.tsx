import DarkModeToggle from 'react-dark-mode-toggle'

type DarkProps = {
	darkMode: boolean | undefined
	toggleDarkMode: () => void
}

const DarkToggle = (props: DarkProps) => {
	const { darkMode, toggleDarkMode } = props

	return (
		<DarkModeToggle onChange={toggleDarkMode} checked={darkMode} size={80} />
	)
}

export default DarkToggle
