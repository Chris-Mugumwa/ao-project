import { SubHeading, Paragraph, PricingCard } from '../../utilities'
import { pricingData } from './'

const Pricing = () => {
	return (
		<section
			id='pricing'
			className='flex-col items-center page bg-gray-light dark:bg-blue-darker py-32'>
			<SubHeading>Start Saving Your Money</SubHeading>

			<Paragraph>
				Choose a plan that works best for you, feel free to contact us if
				you need more details
			</Paragraph>

			<ul className='flex flex-wrap justify-center space-x-4 my-3 w-full mt-24'>
				{pricingData.map(pricing => (
					<PricingCard key={pricing.id} pricing={pricing} />
				))}
			</ul>
		</section>
	)
}

export default Pricing
