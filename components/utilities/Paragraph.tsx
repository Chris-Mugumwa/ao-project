import { PropsWithChildren } from 'react'

const Paragraph = ({ children }: PropsWithChildren) => {
	return (
		<p className='font-outfit text-blue-dark max-w-[90%] dark:text-gray-light lg:text-xl mt-8'>
			{children}
		</p>
	)
}

export default Paragraph
