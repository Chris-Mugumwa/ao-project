import { SubHeading, Paragraph } from '../../utilities'

type WorkProps = {
	works: []
}

type Works = {
	albumId: number
	id: number
	title: string
	url: string
	thumbnailUrl: string
}

const Work = ({ works }: WorkProps) => {
	return (
		<section className='flex-col items-center pt-32 page dark:bg-blue-dark'>
			<SubHeading>Our Work</SubHeading>
			<Paragraph>What we have done for people</Paragraph>

			<div className='flex flex-wrap justify-center gap-4 m-2 mt-12'>
				{works?.map((work: Works) => (
					<div key={work.id} className='w-full lg:w-[30%]'>
						<img
							src={`${work.url}`}
							className='transition duration-500 cursor-pointer hover:scale-105'
						/>
					</div>
				))}
			</div>
		</section>
	)
}

export default Work
