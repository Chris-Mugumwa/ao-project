type Navigation = {
	id: number
	name: string
	path: string
}[]

const navigationData: Navigation = [
	{
		id: 1,
		name: 'About',
		path: '#about',
	},
	{
		id: 2,
		name: 'Team',
		path: '#team',
	},
	{
		id: 3,
		name: 'Projects',
		path: '#work',
	},
	{
		id: 4,
		name: 'Pricing',
		path: '#pricing',
	},
	{
		id: 5,
		name: 'Contact',
		path: '#contact',
	},
]

const pageData: Navigation = [
	{
		id: 1,
		name: 'Work',
		path: '/work',
	},
]

export { navigationData, pageData }
