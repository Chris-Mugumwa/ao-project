import {
	IoLogoFacebook,
	IoLogoInstagram,
	IoLogoSnapchat,
	IoLogoPinterest,
	IoLogoTwitter,
	IoLogoLinkedin,
} from 'react-icons/io5'

type Social = {
	name: string
	icon: JSX.Element
}[]

const socialData: Social = [
	{
		name: 'Facebook',
		icon: <IoLogoFacebook />,
	},
	{
		name: 'Instagram',
		icon: <IoLogoInstagram />,
	},
	{
		name: 'Snapchat',
		icon: <IoLogoSnapchat />,
	},
	{
		name: 'Pinterest',
		icon: <IoLogoPinterest />,
	},
	{
		name: 'Twitter',
		icon: <IoLogoTwitter />,
	},
	{
		name: 'LinkedIn',
		icon: <IoLogoLinkedin />,
	},
]

const Socials = () => {
	return (
		<div className='flex space-x-3 mt-6'>
			{socialData.map(social => (
				<div
					key={social.name}
					className='w-10 h-10 rounded-full bg-gray-light dark:bg-blue-darker flex justify-center items-center transition duration-500 hover:scale-105 cursor-pointer'>
					<span className='text-2xl text-blue-dark dark:text-gray-light'>
						{social.icon}
					</span>
				</div>
			))}
		</div>
	)
}

export default Socials
