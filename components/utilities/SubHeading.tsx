import { PropsWithChildren } from 'react'

const SubHeading = ({ children }: PropsWithChildren) => {
	return (
		<h2 className='font-catamaran text-3xl lg:text-5xl font-medium mb-2 text-blue-dark dark:text-gray-light'>
			{children}
		</h2>
	)
}

export default SubHeading
