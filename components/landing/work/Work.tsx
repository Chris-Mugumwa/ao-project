import { SubHeading, Button } from '../../utilities'

type WorkProps = {
	works: []
}

type Works = {
	albumId: number
	id: number
	title: string
	url: string
	thumbnailUrl: string
}

const Work = ({ works }: WorkProps) => {
	return (
		<section
			id='work'
			className='flex-col items-center justify-center page lg:py-24 lg:min-h-screen'>
			<SubHeading>Check Out Our Work</SubHeading>

			<div className='flex flex-wrap justify-center gap-4 m-2 mt-12'>
				{works?.map((work: Works) => (
					<div key={work.id} className='w-full lg:w-[30%]'>
						<img
							src={`${work.url}`}
							className='transition duration-500 cursor-pointer hover:scale-105'
						/>
					</div>
				))}
			</div>

			<div className='mt-8'>
				<Button path={'/work'}>View More</Button>
			</div>
		</section>
	)
}

export default Work
