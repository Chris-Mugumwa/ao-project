import Image from 'next/image'
import { Heading, Description, Button, Socials } from '../../utilities'
import { Blob } from '../../icons'

const Hero = () => {
	return (
		<header
			id='home'
			className='relative z-30 lg:mt-16 mt-24 h-[calc(100vh-4rem)] page flex flex-wrap'>
			<section className='flex flex-col justify-center gap-2 lg:w-1/2 text-blue-dark dark:text-gray-light'>
				<Heading>Start something that matters</Heading>

				<Description>
					Stop wasting valuable time with projects that just is not you.
				</Description>

				<Button path={'#about'}>Learn More</Button>

				<Socials />
			</section>

			<section className='relative lg:w-1/2 lg:mt-0 '>
				<Image
					src='/Banner-large.png'
					alt='banner'
					width='500'
					height='500'
					priority
					className='relative z-30'
				/>
				<Blob className='absolute z-20 scale-125 -top-10 -right-10' />
			</section>
		</header>
	)
}

export default Hero
