import { PropsWithChildren } from 'react'

const Description = ({ children }: PropsWithChildren) => {
	return (
		<p className='font-merriweather-sans text-lg lg:text-2xl max-w-[80%] flex items-center gap-3 font-light'>
			<span className=' content-["*"] w-1 rounded-full bg-orange-dark h-full' />
			<span>{children}</span>
		</p>
	)
}

export default Description
