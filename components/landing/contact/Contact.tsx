import { useAppDispatch } from '../../../app/hooks'
import { setValue, FormState } from '../../../features/formSlice'
import { useModal } from '../../../hooks'
import { SubHeading } from '../../utilities'
import { Image } from './'
import { Modal } from '../'
import { Field, Form, Formik, ErrorMessage } from 'formik'
import * as yup from 'yup'

const initialValues = {
	name: '',
	surname: '',
	email: '',
	cellNumber: '',
	message: '',
}

const schema = yup.object().shape({
	name: yup
		.string()
		.min(2, 'Name too short')
		.max(30, 'Name too long')
		.required('Required'),
	surname: yup
		.string()
		.min(2, 'Name too short')
		.max(30, 'Name too long')
		.required('Required'),
	email: yup.string().required('Required').email(),
	cellNumber: yup.string().required('Required'),
	message: yup.string().required('Required').min(4, 'Message too short'),
})

const Contact = () => {
	const dispatch = useAppDispatch()
	const { modalOpen, toggleModal } = useModal()

	const submitForm = async (values: FormState) => {
		await dispatch(
			setValue({
				name: values.name,
				surname: values.surname,
				email: values.email,
				cellNumber: values.cellNumber,
				message: values.message,
			}),
		)
		await toggleModal()
	}

	return (
		<>
			<section
				id='contact'
				className='relative z-20 flex-col items-center px-0 page'>
				<SubHeading>Talk to us!</SubHeading>

				<div className='flex flex-wrap w-full h-full py-4'>
					<section className='w-full px-4 lg:px-16 lg:w-1/2'>
						<Formik
							initialValues={initialValues}
							onSubmit={values => submitForm(values)}
							validationSchema={schema}>
							{({ errors, touched }) => (
								<Form
									autoComplete='off'
									className='flex flex-col text-blue-dark dark:text-gray-light'>
									<div className='container'>
										<div className='sub-container'>
											<label htmlFor='name' className='label'>
												Name
											</label>
											<Field
												id='name'
												name='name'
												placeholder='First Name'
												type='text'
												autoComplete='off'
												className='field'
											/>

											{errors.name && touched.name ? (
												<>
													<ErrorMessage
														name='name'
														component='div'
														className='absolute ml-2 text-red-400 right-2'
													/>
												</>
											) : null}
										</div>
										<div className='sub-container'>
											<label htmlFor='surname' className='label'>
												Surname
											</label>
											<Field
												id='surname'
												name='surname'
												placeholder='Last Name'
												type='text'
												autoComplete='off'
												className='field'
											/>

											{errors.surname && touched.surname ? (
												<>
													<ErrorMessage
														name='surname'
														component='div'
														className='absolute ml-2 text-red-400 right-2'
													/>
												</>
											) : null}
										</div>
									</div>
									<div className='container'>
										<div className='sub-container'>
											<label htmlFor='email' className='label'>
												Email
											</label>
											<Field
												id='email'
												name='email'
												placeholder='Email Address'
												type='email'
												autoComplete='off'
												className='field'
											/>
											{errors.email && touched.email ? (
												<>
													<ErrorMessage
														name='email'
														component='div'
														className='absolute ml-2 text-red-400 right-2'
													/>
												</>
											) : null}
										</div>

										<div className='sub-container'>
											<label htmlFor='cellNumber' className='label'>
												Mobile Number
											</label>
											<Field
												id='cellNumber'
												name='cellNumber'
												placeholder='Contact Number'
												type='text'
												autoComplete='off'
												className='field'
											/>
											{errors.cellNumber && touched.cellNumber ? (
												<>
													<ErrorMessage
														name='cellNumber'
														component='div'
														className='absolute ml-2 text-red-400 right-2'
													/>
												</>
											) : null}
										</div>
									</div>

									<div className='flex flex-col w-full gap-2 px-2 my-4'>
										<label
											htmlFor='text area'
											className='mb-1 text-lg font-catamaran text-blue-dark dark:text-gray-light'>
											Message
										</label>
										<Field
											component='textarea'
											id='message'
											name='message'
											rows='8'
											cols='8'
											className='w-full px-4 py-3 rounded-md bg-gray-light dark:bg-blue-darker'
										/>
									</div>

									<button
										type='submit'
										className='px-10 py-3 mt-3 ml-2 transition duration-500 rounded-md bg-orange-dark w-fit hover:bg-transparent ring-2 ring-transparent hover:ring-orange-dark text-gray-light hover:text-blue-dark dark:text-gray-light'>
										Submit
									</button>
								</Form>
							)}
						</Formik>
					</section>

					<Image />
				</div>
				{modalOpen && <Modal toggleModal={toggleModal} />}
			</section>
		</>
	)
}

export default Contact
