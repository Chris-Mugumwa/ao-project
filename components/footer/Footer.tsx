import { IoChevronUp } from 'react-icons/io5'
import { SubHeading, Socials } from '../utilities'

const Footer = () => {
	const scrollTop = () => window.scroll(0, 0)
	return (
		<footer className='flex flex-col items-center px-8 py-10 bg-gray-light dark:bg-blue-darker lg:px-16 lg:py-12'>
			<SubHeading>We hope to hear from you soon</SubHeading>
			<section className='flex items-center justify-between w-full'>
				<Socials />

				<div
					className='flex items-center justify-center w-12 h-12 p-3 transition duration-500 bg-white rounded-full cursor-pointer dark:bg-blue-dark hover:bg-gray-light dark:hover:bg-blue-darker ring-2 ring-orange-dark'
					onClick={scrollTop}>
					<IoChevronUp className='w-8 h-8 text-blue-dark dark:text-gray-light' />
				</div>
			</section>

			<p className='mt-8 font-catamaran text-blue-dark dark:text-gray-100'>
				Powered by
				<a
					href='https://ngoni-mugumwa.vercel.app/'
					target='_blank'
					rel='noreferrer'
					className='underline'>
					Ngonidzashe Mugumwa
				</a>
			</p>
		</footer>
	)
}

export default Footer
