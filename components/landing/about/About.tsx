import Image from 'next/image'
import { SubHeading, Paragraph, Button } from '../../utilities'
import { Blob } from '../../icons'

const About = () => {
	return (
		<>
			<section
				id='about'
				className='flex-col items-center mt-48 page lg:mt-0'>
				<SubHeading>Who are we?</SubHeading>
				<div className='flex flex-wrap-reverse items-center w-full mt-16'>
					<section className='relative mt-24 lg:w-1/2 lg:mt-0'>
						<Image
							src='/Human.png'
							alt='about'
							width='600'
							height='500'
							className='relative z-30'
						/>
						<Blob className='absolute z-20 scale-125 -top-32 -left-32' />
					</section>

					<section className='relative z-30 flex flex-col justify-center lg:w-1/2'>
						<SubHeading>Saving you time & money</SubHeading>

						<Paragraph>
							Lorem ipsum dolor sit amet consectetur adipisicing elit.
							Dolorum in minus accusantium aspernatur sit cum voluptas
							totam autem maiores! Eos doloremque beatae quis numquam
							omnis unde velit soluta sapiente maiores!
						</Paragraph>

						<Button path='#work'>View Work</Button>
					</section>
				</div>
			</section>
		</>
	)
}

export default About
