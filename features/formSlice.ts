import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import type { RootState } from '../app/store'

export type FormState = {
	name: string
	surname: string
	email: string
	cellNumber: string
	message: string
}

export const formSlice = createSlice({
	name: 'form',
	initialState: {
		form: {
			name: '',
			surname: '',
			email: '',
			cellNumber: '',
			message: '',
		},
	},
	reducers: {
		setValue: (state, action: PayloadAction<any>) => {
			state.form = { ...state.form, ...action.payload }
		},
	},
})

export const { setValue } = formSlice.actions

export const selectForm = (state: RootState) => state.form.form
export default formSlice.reducer
