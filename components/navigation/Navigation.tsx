import Link from 'next/link'
import { useState } from 'react'
import { LogoLight, LogoDark } from '../icons/'
import { Menu, navigationData, pageData } from '../'
import { NavLink, PageLink } from '../utilities'
import { useDarkMode, useMenu } from '../../hooks'
import { IoMenuOutline, IoSunnyOutline, IoMoonOutline } from 'react-icons/io5'

const Navigation = () => {
	const [activeLink, setActiveLink] = useState(0)
	const [activePage, setActivePage] = useState(0)
	const { darkMode, toggleDarkMode } = useDarkMode()
	const { menuOpen, toggleMenu } = useMenu()

	const homePage = () => {
		window.scroll(0, 0)
		setActiveLink(0)
		setActivePage(0)
	}

	return (
		<>
			<nav className='fixed top-0 left-0 right-0 z-40 flex items-center justify-between h-16 px-8 py-10 bg-white lg:px-16 lg:py-12 dark:bg-blue-dark'>
				<Link href='/' className='cursor-pointer dark:hidden'>
					<LogoLight className='cursor-pointer dark:hidden' />
				</Link>
				<Link href='/' className='hidden cursor-pointer dark:flex'>
					<LogoDark className='hidden cursor-pointer dark:flex' />
				</Link>

				<div className='flex items-center justify-center p-3 transition duration-500 rounded-md cursor-pointer lg:hidden hover:bg-gray-light dark:hover:bg-blue-darker'>
					<div onClick={toggleMenu}>
						<IoMenuOutline className='w-8 h-8 text-blue-dark dark:text-gray-light' />
					</div>
				</div>

				<ul className='items-center justify-center hidden space-x-8 text-xl text-blue-darker dark:text-gray-light font-catamaran lg:flex'>
					{navigationData.map(navigation => (
						<NavLink
							key={navigation.id}
							navigation={navigation}
							activeLink={activeLink}
							setActiveLink={setActiveLink}
						/>
					))}
				</ul>

				<ul className='items-center justify-center hidden space-x-8 text-xl justify-self-center text-blue-darker dark:text-gray-light font-catamaran lg:flex'>
					{pageData.map(page => (
						<PageLink
							key={page.id}
							page={page}
							activePage={activePage}
							setActivePage={setActivePage}
						/>
					))}

					<div
						onClick={toggleDarkMode}
						className='flex items-center justify-center p-3 mt-1 transition duration-500 rounded-md cursor-pointer hover:bg-gray-light dark:hover:bg-blue-darker'>
						{darkMode && (
							<IoSunnyOutline className='hover:text-yellow-dark' />
						)}
						{!darkMode && (
							<IoMoonOutline className='hover:text-blue-dark' />
						)}
					</div>
				</ul>
			</nav>
			<Menu
				menuOpen={menuOpen}
				toggleMenu={toggleMenu}
				darkMode={darkMode}
				toggleDarkMode={toggleDarkMode}
			/>
		</>
	)
}

export default Navigation
