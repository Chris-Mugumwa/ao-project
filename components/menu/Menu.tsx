import Link from 'next/link'
import { useState } from 'react'
import { menuData } from './'
import { DarkToggle } from '../utilities'
import { IoCloseOutline } from 'react-icons/io5'

type MenuProps = {
	menuOpen: boolean
	toggleMenu: () => void
	darkMode: boolean | undefined
	toggleDarkMode: () => void
}

const Menu = (props: MenuProps) => {
	const [activeLink, setActiveLink] = useState(0)
	const { menuOpen, toggleMenu, darkMode, toggleDarkMode } = props

	const linkClicked = (id: number) => {
		setActiveLink(id)
		toggleMenu()
	}

	return (
		<nav
			className={menuOpen ? 'menu translate-x-0' : 'menu -translate-x-full'}>
			<header className='flex items-center justify-between'>
				<DarkToggle darkMode={darkMode} toggleDarkMode={toggleDarkMode} />

				<IoCloseOutline
					className='w-10 h-10 cursor-pointer text-blue-dark dark:text-gray-light'
					onClick={toggleMenu}
				/>
			</header>

			<ul className='flex flex-col items-center justify-center w-full gap-4 mt-32 text-2xl font-catamaran'>
				{menuData.map(menu => (
					<a
						href={`${menu.path}`}
						key={menu.id}
						onClick={() => linkClicked(menu.id)}
						className={
							menu.id === activeLink
								? 'menu-link ring-orange-dark text-orange-dark dark:text-orange-dark'
								: 'menu-link ring-blue-dark dark:ring-gray-light hover:ring-blue-normal hover:text-blue-normal focus:ring-blue-normal focus:text-blue-normal dark:hover:ring-blue-normal dark:hover:text-blue-normal dark:focus:ring-blue-normal dark:focus:text-blue-normal'
						}>
						<span>{menu.icon}</span>
						<span>{menu.name}</span>
					</a>
				))}

				<Link href='/work'>
					<a
						className='flex items-center justify-center w-full px-6 py-4 space-x-2 transition duration-500 rounded-md text-blue-dark dark:text-gray-light ring-2'
						onClick={toggleMenu}>
						Work
					</a>
				</Link>
			</ul>
		</nav>
	)
}

export default Menu
