import Head from 'next/head'
import { Work } from '../components/works'

type Works = {
	works: []
}

const work = ({ works }: Works) => {
	return (
		<section className='min-h-screen'>
			<Head>
				<title>Work</title>
				<meta name='work' content='Work by NM' />
				<link rel='icon' href='/Logo.svg' />
			</Head>

			<Work works={works} />
		</section>
	)
}

export async function getStaticProps() {
	const response = await fetch(
		'https://jsonplaceholder.typicode.com/photos?_page=1&_limit=9',
	)
	const formatResponse = await response.json()

	return {
		props: {
			works: formatResponse,
		},
	}
}

export default work
