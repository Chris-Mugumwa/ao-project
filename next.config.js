/** @type {import('next').NextConfig} */
const nextConfig = {
	reactStrictMode: true,
	swcMinify: true,
	images: {
		domains: ['https://via.placeholder.com/600/'],
	},
}

module.exports = nextConfig
