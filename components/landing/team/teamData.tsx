type Team = {
	name: string
	role: string
	photo: string
}[]

const teamData: Team = [
	{
		name: 'Andrew Young',
		role: 'Chief Executive Officer',
		photo: '/employee-1.jpg',
	},
	{
		name: 'Martha James',
		role: 'Chief Technology Officer',
		photo: '/employee-2.jpg',
	},
	{
		name: 'Jennifer Stone',
		role: 'Software Engineer',
		photo: '/employee-3.jpg',
	},
	{
		name: 'Marcus Williams',
		role: 'Head of Marketing',
		photo: '/employee-4.jpg',
	},
]

export default teamData
