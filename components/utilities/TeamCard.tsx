import Image from 'next/image'
import { IoMailOutline, IoLogoLinkedin } from 'react-icons/io5'

type Team = {
	team: {
		name: string
		role: string
		photo: string
	}
}

const TeamCard = ({ team }: Team) => {
	return (
		<div className='flex flex-col items-center justify-center w-full gap-3 px-4 py-10 my-2 transition duration-500 bg-white rounded-md dark:bg-blue-dark md:w-56'>
			<Image
				src={`${team.photo}`}
				alt={`${team.name}`}
				width='100'
				height='100'
				className='rounded-full'
			/>

			<h2 className='text-xl text-orange-dark font-catamaran'>
				{team.name}
			</h2>
			<h5 className='text-blue-dark dark:text-gray-light font-merriweather-sans'>
				{team.role}
			</h5>
		</div>
	)
}

export default TeamCard
