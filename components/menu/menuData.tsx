import {
	IoInformationOutline,
	IoPeopleOutline,
	IoCallOutline,
	IoBriefcaseOutline,
	IoWalletOutline,
} from 'react-icons/io5'

type Menu = {
	id: number
	name: string
	path: string
	icon: JSX.Element
}[]

const menuData: Menu = [
	{
		id: 1,
		name: 'About',
		path: '#about',
		icon: <IoInformationOutline />,
	},
	{
		id: 2,
		name: 'Team',
		path: '#team',
		icon: <IoPeopleOutline />,
	},
	{
		id: 3,
		name: 'Projects',
		path: '#work',
		icon: <IoBriefcaseOutline />,
	},
	{
		id: 4,
		name: 'Pricing',
		path: '#pricing',
		icon: <IoWalletOutline />,
	},
	{
		id: 5,
		name: 'Contact',
		path: '#contact',
		icon: <IoCallOutline />,
	},
]

export default menuData
