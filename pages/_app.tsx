import '../styles/globals.css'
import { Provider } from 'react-redux'
import type { AppProps } from 'next/app'
import { store } from '../app/store'
import { Navigation, Footer } from '../components'

function MyApp({ Component, pageProps }: AppProps) {
	return (
		<Provider store={store}>
			<main className='relative'>
				<Navigation />
				<Component {...pageProps} />
				<Footer />
			</main>
		</Provider>
	)
}

export default MyApp
