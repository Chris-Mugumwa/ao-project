import Head from 'next/head'
import {
	Hero,
	About,
	Team,
	Work,
	Pricing,
	Contact,
} from '../components/landing'

type Works = {
	works: []
}

const Home = ({ works }: Works) => {
	return (
		<div className='relative min-h-screen overflow-x-hidden transition-all duration-1000 scrollbar dark:bg-blue-dark'>
			<Head>
				<title>AO Technology Group Project</title>
				<meta
					name='description'
					content='Project for AO Technology Group by Ngoni Mugumwa'
				/>
				<link rel='icon' href='/Logo.svg' />
			</Head>

			<Hero />
			<About />
			<Team />
			<Work works={works} />
			<Pricing />
			<Contact />
		</div>
	)
}

export async function getStaticProps() {
	const response = await fetch(
		'https://jsonplaceholder.typicode.com/photos?_page=1&_limit=3',
	)
	const formatResponse = await response.json()

	return {
		props: {
			works: formatResponse,
		},
	}
}

export default Home
