import { PropsWithChildren } from 'react'

const Heading = ({ children }: PropsWithChildren) => {
	return (
		<h1 className='font-catamaran text-4xl  lg:text-6xl font-medium  mb-2'>
			{children}
		</h1>
	)
}

export default Heading
