import { useAppSelector } from '../../../app/hooks'
import { selectForm } from '../../../features/formSlice'
import Avatar from 'react-avatar'
import { motion } from 'framer-motion'
import { IoCloseOutline } from 'react-icons/io5'

type ModalProps = {
	toggleModal: () => void
}

const Modal = ({ toggleModal }: ModalProps) => {
	const form = useAppSelector(selectForm)

	return (
		<motion.section
			initial={{ opacity: 0 }}
			animate={{ opacity: 1 }}
			exit={{ opacity: 0 }}
			className='absolute top-0 z-50 flex items-center justify-center w-screen h-full px-8 bg-black-shaded'>
			<div className='relative flex flex-col items-center px-6 py-10 rounded-md bg-gray-light dark:bg-blue-darker'>
				<div className='absolute z-30 flex items-center justify-center min-w-96 p-3 transition duration-500 bg-white rounded-full cursor-pointer dark:bg-blue-dark hover:bg-gray-light dark:hover:bg-blue-darker -top-6 -right-6'>
					<div onClick={toggleModal}>
						<IoCloseOutline className='w-8 h-8 text-blue-dark dark:text-gray-light' />
					</div>
				</div>
				<Avatar
					name={`${form.name} ${form.surname}`}
					className='rounded-md'
				/>

				<div className='flex flex-col items-center w-full py-2'>
					<div className='flex justify-center w-full mt-4 space-x-6'>
						<div className='flex flex-col items-center'>
							<label
								htmlFor='name'
								className='text-orange-dark font-catamaran'>
								Name
							</label>
							<h4 className='text-2xl font-outfit text-blue-dark dark:text-gray-light'>
								{form.name}
							</h4>
						</div>

						<div className='flex flex-col items-center'>
							<label
								htmlFor='name'
								className='text-orange-dark font-catamaran'>
								surname
							</label>
							<h4 className='text-2xl font-outfit text-blue-dark dark:text-gray-light'>
								{form.surname}
							</h4>
						</div>
					</div>

					<div className='flex flex-col justify-center w-full mt-4'>
						<div className='flex flex-col items-center justify-center'>
							<label
								htmlFor='name'
								className='text-orange-dark font-catamaran'>
								Email
							</label>
							<h4 className='text-2xl font-outfit text-blue-dark dark:text-gray-light'>
								{form.email}
							</h4>
						</div>

						<div className='flex flex-col items-center mt-2'>
							<label
								htmlFor='name'
								className='text-orange-dark font-catamaran'>
								Cell Number
							</label>
							<h4 className='text-2xl font-outfit text-blue-dark dark:text-gray-light'>
								{form.cellNumber}
							</h4>
						</div>
					</div>

					<div className='flex flex-col items-center mt-4'>
						<label
							htmlFor='name'
							className='text-orange-dark font-catamaran'>
							Message
						</label>
						<h4 className='text-2xl font-outfit text-blue-dark dark:text-gray-light'>
							{form.message}
						</h4>
					</div>
				</div>
			</div>
		</motion.section>
	)
}

export default Modal
