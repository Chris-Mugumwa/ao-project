import Link from 'next/link'
import { Dispatch, SetStateAction } from 'react'

type PageProps = {
	page: {
		id: number
		name: string
		path: string
	}
	activePage: number
	setActivePage: Dispatch<SetStateAction<number>>
}

const PageLink = (props: PageProps) => {
	const { page, activePage, setActivePage } = props

	return (
		<Link
			href={`${page.path}`}
			key={page.name}
			onClick={() => setActivePage(page.id)}
			className='button'>
			<a className='button'>
				<span
					className={activePage === page.id ? 'flex dot' : 'dot opacity-0'}
				/>
				{page.name}
			</a>
		</Link>
	)
}

export default PageLink
