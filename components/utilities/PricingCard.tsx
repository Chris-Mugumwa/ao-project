import { IoCheckboxOutline } from 'react-icons/io5'

type PricingProps = {
	pricing: {
		id: number
		name: string
		icon: JSX.Element
		price: string
		perk: {
			perkOne: string
			perkTwo: string
			perkThree: string
		}
	}
}

const PricingCard = ({ pricing }: PricingProps) => {
	return (
		<div className='flex flex-col items-center w-full gap-2 px-6 py-10 mt-4 bg-white rounded-md dark:bg-blue-dark text-blue-dark dark:text-gray-light lg:w-64 h-fit lg:items-start'>
			<h2 className='flex items-center gap-2 text-3xl font-catamaran'>
				<span>{pricing.icon}</span>
				<span>{pricing.name}</span>
			</h2>

			<h3 className='mb-3 text-3xl font-outfit text-orange-dark'>
				{pricing.price}
			</h3>

			<p className='flex items-center gap-2 font-merriweather-sans'>
				<IoCheckboxOutline />
				<span>{pricing.perk.perkOne}</span>
			</p>

			<p className='flex items-center gap-2 font-merriweather-sans'>
				<IoCheckboxOutline />
				<span>{pricing.perk.perkTwo}</span>
			</p>

			<p className='flex items-center gap-2 font-merriweather-sans'>
				<IoCheckboxOutline />
				<span>{pricing.perk.perkThree}</span>
			</p>

			<button className='w-1/2 px-4 py-3 mt-4 transition duration-500 rounded-md lg:w-full bg-orange-dark ring-2 ring-transparent hover:ring-orange-dark hover:bg-transparent font-catamaran text-gray-light hover:text-blue-dark dark:hover:text-gray-light'>
				Select
			</button>
		</div>
	)
}

export default PricingCard
