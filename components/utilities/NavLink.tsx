import Link from 'next/link'
import { Dispatch, SetStateAction } from 'react'

type LinkProps = {
	navigation: {
		id: number
		name: string
		path: string
	}
	activeLink: number
	setActiveLink: Dispatch<SetStateAction<number>>
}

const NavLink = (props: LinkProps) => {
	const { navigation, activeLink, setActiveLink } = props

	return (
		<a
			href={`${navigation.path}`}
			key={navigation.name}
			onClick={() => setActiveLink(navigation.id)}
			className={
				activeLink === navigation.id
					? 'text-orange-dark font-medium transition duration-500'
					: 'transition duration-500'
			}>
			<span className='flex flex-col items-center justify-center gap-1'>
				<span
					className={
						activeLink === navigation.id ? 'flex dot' : 'dot opacity-0'
					}
				/>
				{navigation.name}
			</span>
		</a>
	)
}

export default NavLink
